program gauss

  implicit none

  real, dimension(3,3) :: A
  real, dimension(3) :: b

  integer :: i,j
	

  ! initialize matrix A and vector b
  A(1,:) = (/2., 3., -1./)
  A(2,:) = (/4., 7., 1./)
  A(3,:) = (/7., 10., -4./)

  b = (/1., 3., 4./)
  
  ! open solution text file to write into
  open(10, FILE = 'soln.txt')
    
    
    		
  ! print augmented matrix
  do i = 1, 3           ! i is row
    print *, A(i,:), "|", b(i)
    write(10, *) A(i,:), "|", b(i)
  end do
  print *, ""    ! print a blank line
  write(10, *) ""
  print *, "Gaussian elimination........"
  write(10, *) "Gaussian elimination........"
  
  ! gaussian elimination
  do j = 1, 2           ! j is column
    do i = j+1, 3       ! i is row
    
		!update b
      b(i) = b(i) - A(i,j)/A(j,j)*b(j)
      
      	!update A row by row
      A(i,:) = A(i,:) - A(i,j)/A(j,j)*A(j,:)
    
      
    end do
  end do

  ! print augmented matrix again
  ! this should be an echelon form (or triangular form)
  print *, "***********************"
  write(10, *) "*********************"
  do i = 1, 3
    print *, A(i,:), "|", b(i)
    write(10, *) A(i,:), "|", b(i)
  end do

  print *, ""    ! print a blank line
  print *, "back subs......"
  write(10, *) ""    ! print a blank line
  write(10, *) "back subs......"



  ! doing back substitution
  do j = 3, 2, -1        ! j is column
    do i = j-1, 1, -1    ! i is row
      
      b(i) = b(i) - b(j)*A(i,j)/A(j,j)
      
      A(i,:) = A(i,:) - A(j,:)*A(i,j)/A(j,j)
      
    end do
  end do

  ! print the results
  print *, "***********************"
  write(10, *) "*********************"
  do i = 1, 3
    print *, A(i,:), "|", b(i)
    write(10, *) A(i,:), "|", b(i)
  end do


	
  ! calculate solution vector
  do i = 1, 3
  	b(i) = b(i)/A(i,i)
  end do
  
  ! print solution vector 
  print *, ""    ! print a blank line
  print *, "The solutions are:"
  
  write(10, *) ""    ! print a blank line
  write(10, *) "The solutions are:"
  
  
  do i = 1, 3
  	print *, b(i)
  	write(10, *) b(i)
  end do



  ! close solution text file
  close(10)
  
end program gauss




























