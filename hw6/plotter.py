import numpy as np
import matplotlib.pyplot as plt
import os

def word_count(file_path):
	file_name = os.path.basename(file_path)
	raw_data = open(file_name, 'r')
	s = raw_data.read()

	s = s.replace(' ','')
	s = s.replace('\n','')


	# initialize empty dictionary
	d = dict()

	for c in s:
		d[c] = d.get(c,0) + 1

	return d



if __name__ == "__main__":
	file_path = './words.txt'
	his = word_count(file_path)
	
	# create list with keys
	keys = sorted(his)
	
	# create numpy array with values
	nCounts = np.array([])
	for c in keys:
		nCounts = np.append(nCounts, his[c])
	

	# plot
	fig = plt.figure(figsize=(7,5))
	x = np.arange(len(his.keys()))
	fig, plt.bar(x,nCounts)
	fig, plt.plot(keys, nCounts, color='red', marker='o')
	plt.xticks(keys)
	plt.xlabel('Characters')
	plt.ylabel('Frequency')
	plt.grid()
	fig.savefig("result.png")

	plt.show()