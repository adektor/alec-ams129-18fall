import numpy as np
import matplotlib.pyplot as plt
import os

if __name__ == "__main__":

	# Compile and run Fortran code
	os.chdir("../fortran")
	os.system('make')
	os.system('./main.exe')

	# Load Data
	file_path = '../fortran/output_8.txt'
	file_name = os.path.basename(file_path)
	data_8 = np.loadtxt(file_name)

	file_path = '../fortran/output_16.txt'
	file_name = os.path.basename(file_path)
	data_16 = np.loadtxt(file_name)
	
	file_path = '../fortran/output_32.txt'
	file_name = os.path.basename(file_path)
	data_32 = np.loadtxt(file_name)
	
	file_path = '../fortran/output_64.txt'
	file_name = os.path.basename(file_path)
	data_64 = np.loadtxt(file_name)


	# Change directory back
	os.chdir("../python")


	# calculate errors
	h = 10.0/7.0
	t = np.arange(0.0, 10.0, h)
	y = -np.sqrt(2*np.log(t**2 + 1) + 4)
	err_8 = 0
	for x in range(0,len(t)):
		err_8 = err_8 + abs(y[x]-data_8[x,1])

	h = 10.0/15.0
	t = np.arange(0.0, 10.0, h)
	y = -np.sqrt(2*np.log(t**2 + 1) + 4)
	err_16 = 0
	for x in range(0,len(t)):
		err_16 = err_16 + abs(y[x]-data_16[x,1])


	h = 10.0/31.0
	t = np.arange(0.0, 10.0, h)
	y = -np.sqrt(2*np.log(t**2 + 1) + 4)
	err_32 = 0
	for x in range(0,len(t)):
		err_32 = err_32 + abs(y[x]-data_32[x,1])


	h = 10.0/63.0
	t = np.arange(0.0, 10.0, h)
	y = -np.sqrt(2*np.log(t**2 + 1) + 4)
	err_64 = 0
	for x in range(0,len(t)):
		err_64 = err_64 + abs(y[x]-data_64[x,1])



	# analytic solution
	t = np.arange(0.0, 10.0, 0.1)
	y = -np.sqrt(2*np.log(t**2 + 1) + 4)

	# plot
	fig = plt.figure(figsize=(7,5))
	plt.plot(data_8[:,0], data_8[:,1], color='red', linestyle='--',marker='o')
	plt.plot(t,y, color='blue', linestyle='-')
	plt.title(err_8)
	plt.xlabel('t axis')
	plt.ylabel('y axis')
	plt.grid()
	fig.savefig("result_8.png")

	fig = plt.figure(figsize=(7,5))
	plt.plot(data_16[:,0], data_16[:,1], color='red', linestyle='--',marker='o')
	plt.plot(t,y, color='blue', linestyle='-')
	plt.title(err_16)
	plt.xlabel('t axis')
	plt.ylabel('y axis')
	plt.grid()
	fig.savefig("result_16.png")

	fig = plt.figure(figsize=(7,5))
	plt.plot(data_32[:,0], data_32[:,1], color='red', linestyle='--',marker='o')
	plt.plot(t,y, color='blue', linestyle='-')
	plt.title(err_32)
	plt.xlabel('t axis')
	plt.ylabel('y axis')
	plt.grid()
	fig.savefig("result_32.png")


	fig = plt.figure(figsize=(7,5))
	plt.plot(data_64[:,0], data_64[:,1], color='red', linestyle='--',marker='o')
	plt.plot(t,y, color='blue', linestyle='-')
	plt.title(err_64)
	plt.xlabel('t axis')
	plt.ylabel('y axis')
	plt.grid()
	fig.savefig("result_64.png")





