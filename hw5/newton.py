
# Use right_justify from hw4 for data output format
def right_justify(StringInput, space):
	
	# get string length
	length = len(StringInput)
	# initialize output
	StringOutput = ''
	# initialize counter
	i = 0
	
	# inside loop we add the appropriate number of spaces
	while i < space - len(StringInput):
		StringOutput += ' ' 
		i += 1
		
	# append the StringInput
	StringOutput += StringInput	
	return(StringOutput)



# Newtons method
# INPUTS:
# f ---> function of which we want a root (single variable)
# df --> derivative of f
# initial_guess ---> guess of where we may find a root
# threshold ---> how close we would like to get to the analytic root

# prints iteration data to screen
def newtons(f, df, initial_guess, threshold):
	x0 = initial_guess

	# initialize a large dx
	dx = 10.0

	# initialize counter for iterations
	n = 0

	# print initial data
	print(right_justify('n',3)+"|"+right_justify('x',22)+"|"+right_justify('dx',22))
	print('-------------------------------------------------')
	print(right_justify(str(n),3)+"|"+right_justify(str(x0),22)+"|"+right_justify(str(dx),22))

	# update our approximate root until we get within threshold
	while (abs(dx) > threshold):
		x1 = x0-(f(x0)/df(x0))
		dx = abs(x1 - x0)
		x0 = x1
		n = n + 1

		#print data from iteration
		print(right_justify(str(n),3)+"|"+right_justify(str(x0),22)+"|"+right_justify(str(dx),22))
