import newton

# define the function of which we want to find the root
def f(x):
	f = 2*x**7 + 4*x**5 - 2*x**3 + 3*x + 1
	return(f)

# define the derivative of the function of which we want to find the root
def df(x):
	df = 14*x**6 + 20*x**4 - 6*x**2 + 3
	return(df)

# main block -- call newtons on our function
if __name__ == "__main__":
	newton.newtons(f, df, 10.0, 10**(-8))