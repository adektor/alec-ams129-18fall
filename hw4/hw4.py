def right_justify(StringInput):
	
	# get string length
	length = len(StringInput)
	# initialize output
	StringOutput = ''
	# initialize counter
	i = 0
	
	# inside loop we add the appropriate number of spaces
	while i < 70 - len(StringInput):
		StringOutput += ' ' 
		i += 1
		
	# append the StringInput
	StringOutput += StringInput
	print(StringOutput)		
	return 


def count_char(s, c):
	# store upper case character
	C = c.upper()
	# initialize counter
	count = 0
	
	# loop through characters in the string s and count
	for letter in s :
		if (letter == c or letter == C):
			count += 1
		
	print(count)	
	return
	
def cumulative_sum(numlist):
	# initialize list to store sum
	numsum = []
	
	# initialize counter
	i = 0
	for n in numlist:
		if i > 0:
			numsum.append(n + numsum[i-1])
		else:
			numsum.append(n)		
		i += 1
		

	return(numsum)
	
	
def check_palindrome(string):
	# get length of string	
	n = len(string)
	
	# initialize counter
	i = 0
	
	# initialize boolean to true
	is_pal = True
	
	# check recursively for palindrome
	while i <= n-1:
		if string[i] != string[n-i-1]:
			is_pal = False
		i += 1
	return(is_pal)
	
	
	

if __name__ == "__main__":
	right_justify('Alec Dektor')    # please put your real name

	lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis tellus id mollis scelerisque. In consequat nec tellus lacinia iaculis. Mauris auctor volutpat aliquam. Nulla dignissim arcu placerat tellus pretium, vel venenatis sem porta. Donec interdum tincidunt mi, et convallis velit aliquet eget. Nam id rutrum felis. Fusce vel fermentum justo. Pellentesque faucibus orci at velit vehicula dignissim. Duis faucibus dapibus malesuada. Pellentesque iaculis tristique vestibulum. Sed egestas nisl non augue imperdiet mattis. Nunc vitae purus lectus. Vestibulum mi turpis, volutpat vel odio quis, hendrerit suscipit ligula. Nunc in massa diam. Suspendisse aliquam quam et ex egestas vestibulum vitae id libero. Cras molestie consectetur condimentum.'
	count_char(lorem, 't')

	result = cumulative_sum([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
	print(result)

	words_list = ['noon', 'madam', 'ams129', 'redivider', 'numpy', 'bob', 'racecar', 'youngjun']
	for word in words_list:
    		if(check_palindrome(word)):
        		print(word, 'is palindrome!')
        	
        	
        	
        	
        	
        	
        	
        	
        	
